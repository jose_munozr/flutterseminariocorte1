import 'package:flutter/material.dart';
import 'package:libreria_seminario/src/models/Libro.dart';

class LibroListItem extends StatelessWidget {
  //const LibroListItem({Key key}) : super(key: key);
  final Libro libro;
  LibroListItem(this.libro);
  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Column(
        children: <Widget>[
          new Container(
            child: new Text(libro.nombre),
          )
        ],
      ),
    );
  }
}
