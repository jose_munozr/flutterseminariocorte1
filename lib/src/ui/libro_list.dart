import 'package:flutter/material.dart';
import 'package:libreria_seminario/src/common/httpHandler.dart';
import 'package:libreria_seminario/src/models/Libro.dart';
import 'package:libreria_seminario/src/ui/libro_list_item.dart';

class LibroList extends StatefulWidget {
  LibroList({Key key}) : super(key: key);

  _LibroListState createState() => _LibroListState();
}

class _LibroListState extends State<LibroList> {
  List<Libro> _libro = new List();
  @override
  void initState() {
    super.initState();
    loadLibros();
  }

  void loadLibros() async {
    var libros = await HttpHandler().fetchLibros();
    setState(() {
      _libro.addAll(libros);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new ListView.builder(
        itemCount: _libro.length,
        itemBuilder: (BuildContext context, int index) {
          return new LibroListItem(_libro[index]);
        },
      ),
    );
  }
}
