import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:libreria_seminario/src/blocs/authentication/authentication.dart';
import 'package:libreria_seminario/src/ui/libro_list.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Inicio'),
        backgroundColor: Colors.purple,
      ),
      drawer: new Drawer(
          child: new ListView(
        children: <Widget>[
          new DrawerHeader(
            child: new Material(),
          ),
          new ListTile(
            title: new Text('Libros'),
            trailing: new Icon(Icons.book),
          ),
          new Divider(
            height: 5.0,
          ),
          new ListTile(
            title: new Text('Log out'),
            trailing: new Icon(Icons.close),
            onTap: () {
              authenticationBloc.dispatch(LoggedOut());
            },
          ),
        ],
      )),
      body: new Container(
        child: new Center(
          child: new PageView(
            children: <Widget>[new LibroList()],
          ),
        ),
      ),
    );
  }
}
