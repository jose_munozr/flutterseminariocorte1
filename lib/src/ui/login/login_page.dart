import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:libreria_seminario/src/blocs/login/login.dart';
import 'package:libreria_seminario/src/resources/UserRepository.dart';
import 'package:libreria_seminario/src/blocs/authentication/authentication.dart';
import 'package:libreria_seminario/src/ui/login/login_form.dart';

class LoginPage extends StatelessWidget {
  final UserRepository userRepository;

  LoginPage({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Login'),
        backgroundColor: Colors.purple,
      ),
      body: new BlocProvider(
          builder: (context) {
            return LoginBloc(
              authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
              userRepository: userRepository,
            );
          },
          child: new Center(
            child: LoginForm(),
          )),
    );
  }
}
