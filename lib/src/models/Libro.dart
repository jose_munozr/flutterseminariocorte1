class Libro {
  int id;
  String nombre;
  String autor;
  String editorial;

  factory Libro(Map jsonMap) {
    try {
      return new Libro.deserialize(jsonMap);
    } catch (ex) {
      throw ex;
    }
  }

  Libro.deserialize(Map json)
      : id = json['id'].toInt,
        nombre = json['nombre'].toString(),
        autor = json['autor'].toString(),
        editorial = json['editorial'].toString();
}
