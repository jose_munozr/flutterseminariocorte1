import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:libreria_seminario/src/models/Libro.dart';

class HttpHandler {
  final String _baseUrl =
      'ec2-18-188-79-173.us-east-2.compute.amazonaws.com:9090';

  Future<dynamic> getJson(Uri uri) async {
    http.Response response = await http.get(uri);
    return json.decode(response.body);
  }

  Future<List<Libro>> fetchLibros() {
    var uri = new Uri.http(_baseUrl, '/libros');
    return getJson(uri).then(((data) =>
        data['results'].map<Libro>((item) => new Libro(item)).toList()));
  }
}
